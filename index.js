//Import thư viện expressjs tương đương import express from "express"; 
const express = require("express");

//Khởi tạo 1 app express 
const app = express();

//Khai báo cổng chạy project
const port = 8000;

//Callback function là 1 function đóng vai trò là tham số của 1 function khác, nó sẽ được thực hiện khi function chủ được gọi
// Khai báo API dạng /
app.get("/", (req, res) => {
    let today = new Date();

    res.json({
        message: `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth()+1} năm ${today.getFullYear()}`
    })
})

//Khai báo API dạng GET
app.get("/get-method", (req, res) => {
    res.json({
        message: "GET Method"
    })
})

//Khai báo API dạng POST
app.post("/post-method", (req, res) => {
    res.json({
        message: "POST Method"
    })
})

//Khai báo API dạng PUT
app.put("/put-method", (req, res) => {
    res.json({
        message: "PUT Method"
    })
})

//Khai báo API dạng DELETE
app.delete("/delete-method", (req, res) => {
    res.json({
        message: "DELETE Method"
    })
})

app.listen(port, () => {
    console.log("App listening on port: ", port);
})